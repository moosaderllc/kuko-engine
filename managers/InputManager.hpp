// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_INPUTMANAGER
#define _KUKO_INPUTMANAGER

#include <SDL.h>

#include <map>
#include <string>

#include "../base/PositionRect.hpp"

namespace kuko
{

// TODO: Add "registration" of keys for commands.
enum CommandButton { TAP, SECONDARY_TAP, MOUSE_DOWN, MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, BACKSPACE, WINDOW_CLOSE  };

//! Information on the Action occuring, such as timeout info, key down info, and (x, y) info, as relevant
class TriggerInfo
{
    public:
    TriggerInfo() { timeout = 0; maxTimeout = 50; }

    bool down;
    int actionX, actionY;
    int timeout;
    int maxTimeout;
};

//! Manager to handle mouse, keyboard, and gamepad input
class InputManager
{
    public:
    static void Setup();
    static void Cleanup();

    static void Update();
    static void IgnoreMouseUntilButtonUp();

    static std::map<CommandButton, TriggerInfo> GetTriggerInfo();
    static std::string GetTextInputBuffer();
    static void SetTextBufferActive( bool val );

    static IntRect GetMousePosition();

    static std::string GetKeyHit();

    protected:
    static void ResetTriggers();

    //! The SDL Event object
    static SDL_Event m_event;
    //! Information on whether a certain command was triggered
    static std::map<CommandButton, TriggerInfo> m_eventTriggered;

    //! Text buffer for input text boxes
    static char m_textInputBuffer[256];
    //! The last key hit, for use with text boxes
    static std::string m_lastKeyHit;

    //! Whether text input is enabled
    static bool m_enableTextInput;
};

}

#endif

