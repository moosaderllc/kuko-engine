// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "LanguageManager.hpp"

#ifdef NOLUA
#include "interfaces/LualessLanguage.hpp"
#else
#include "interfaces/LuaLanguage.hpp"
#endif
#include "../utilities/Logger.hpp"

namespace kuko
{

ILanguage* LanguageManager::m_language;

//! Initialize the language manager to use the LuaLanguage object or the LualessLanguage object
void LanguageManager::Setup()
{
    m_language = NULL;
    #ifdef NOLUA
        Logger::Out( "Setup LualessLanguage", "LanguageManager::Setup" );
        m_language = new LualessLanguage;
    #else
        Logger::Out( "Setup LuaLanguage", "LanguageManager::Setup" );
        m_language = new LuaLanguage;
    #endif
}

//! Clean up the Language handler
void LanguageManager::Cleanup()
{
    if ( m_language != NULL )
    {
        delete m_language;
        m_language = NULL;
    }
}

//! Add a language to the program's languages
/**
@param  key     The unique identifier of the language
@param  path    The path to the language file
*/
void LanguageManager::AddLanguage( const std::string& key, const std::string& path )
{
    Logger::Out( "Add language \"" + key + "\" from path \"" + path + "\"", "LanguageManager::AddLanguage" );
    m_language->AddLanguage( key, path );
}

//! Retrieve the current language's identifier
/**
@param          Returns the key of the current language in use
*/
std::string LanguageManager::CurrentLanguage()
{
    return m_language->CurrentLanguage();
}

//! Retrieve the localized text associated with some key
/**
@param  key     The unique identifier of a text string to retrieve
@return         The localized string for that key, or NOTFOUND if not found
*/
std::string LanguageManager::Text( const std::string& key )
{
    return m_language->Text( key );
}

//! Retrieve the localized text associated with some key
/**
@param  langType    The specific language to load this string from
@param  key         The unique identifier of a text string to retrieve
@return             The localized string for that key, or NOTFOUND if not found
*/
std::string LanguageManager::Text( const std::string& langType, const std::string& key )
{
    std::string value = m_language->Text( langType, key );
    return m_language->Text( langType, key );
}

//! Get the suggested font file for this language
/**
@return     The suggested font to be used for the current language
*/
std::string LanguageManager::GetSuggestedFont()
{
    return m_language->GetSuggestedFont();
}

std::string LanguageManager::SpecialField( const std::string& langType, const std::string& field, const std::string& key )
{
    return m_language->SpecialField( langType, field, key );
}

std::string LanguageManager::SpecialField( const std::string& langType, const std::string& field, int key )
{
    return m_language->SpecialField( langType, field, key );
}

int LanguageManager::SpecialFieldCount( const std::string& langType, const std::string& field )
{
    return m_language->SpecialFieldCount( langType, field );
}

}
