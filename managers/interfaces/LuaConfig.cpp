#include "LuaConfig.hpp"

#ifndef NOLUA

#include "../LuaManager.hpp"
#include "../../utilities/StringUtil.hpp"
#include "../../utilities/Logger.hpp"

namespace kuko
{

LuaConfig::LuaConfig()
{
}

//! Loads the Config file (or creates one if none is available), tries to load any existing save data.
/**
@param      settings        A set of config "key values" expected to be read in the file. Also used to create default config file.
@return     true if a config exists, false if no config exists
*/
bool LuaConfig::LoadConfig( const std::vector<std::string>& settings )
{
    Logger::Out( "Load config file", "LuaConfig::LoadConfig" );
    if ( !LuaManager::LoadScript( "config.lua" ) )
    {
        Logger::Out( "Config file not found, create a new one", "LuaConfig::LoadConfig" );
        CreateNewConfig( settings );
        return false;
    }

    // Load in the config values. Make sure you set the expected settings to load
    // in your game file. e.g., configSettings.push_back( "savegame_count" );
    for ( unsigned int i = 0; i < settings.size(); i++ )
    {
        SetOption( settings[i], LuaManager::Config_GetOption( settings[i] ) );
    }

    if ( GetOption( "savegame_count" ) != "" )
    {
        // Get the current amount of saved games from the config file
        std::string savegameCountOption = GetOption( "savegame_count" );
        m_totalSavegames = StringUtil::StringToInt( savegameCountOption );
        Logger::Out( "There are " + savegameCountOption + " save game(s).", "LuaConfig::LoadConfig" );
    }
    else
    {
        Logger::Out( "There are no save game files (yet)!", "LuaConfig::LoadConfig" );
        m_totalSavegames = 0;
    }

    for ( int i = 1; i <= m_totalSavegames; i++ )
    {
        std::string opt = "savegame_" + StringUtil::IntToString( i );
        SetOption( opt, LuaManager::Config_GetOption( opt ) );
    }

    return true;
}

//! Sets an option in the config file.
/**
@param      key     The key of the option
@param      val     The value of the option
*/
void LuaConfig::SetOption( const std::string& key, const std::string& val )
{
    Logger::Out( "Set config option \"" + key + "\" to value \"" + val + "\".", "LuaConfig::SetOption", "config" );
    m_settings[ key ] = val;
}

//! Retrieve a config option with the given key and return its value
/**
@param      key     The key of the option
@return     The value of the option matching the key
*/
std::string LuaConfig::GetOption( const std::string& key )
{
    return m_settings[ key ];
}

/*
    I need to do research on whether the
    standard lua lib has functionality for writing out lua.
*/

//! Saves the config file and the currently stored options
void LuaConfig::SaveConfig()
{
    Logger::Out( "Write out config file", "LuaConfig::SaveConfig", "config" );

    std::ofstream out;
    out.open( "config.lua" );

    out << "config = {" << std::endl;

    for ( auto& option: m_settings )
    {
        out << "\t" << option.first << " = \"" << option.second << "\"," << std::endl;
    }

    out << "}" << std::endl;

    out.close();
}

//! Create a new config file using the settings (keys) passed in. Values will be defaulted to empty strings.
/**
@param      settings        A vector of keys the config file will have
*/
void LuaConfig::CreateNewConfig( const std::vector<std::string>& settings )
{
    Logger::Out( "Creating new config file", "LuaConfig::CreateNewConfig", "config" );
    for ( auto& option: settings )
    {
        SetOption( option, "" );
    }
    SaveConfig();
}

//! Creates a new save file and stores information about the save file in the config.lua file too
/**
@param      playername      The name of the player (Save game will be named "playername-save.lua"
@param      settings        The list of settings (keys) to output to this save file
*/
void LuaConfig::CreateNewSave( const std::string& playername, std::map<std::string, std::string>& settings )
{
    Logger::Out( "Create a new save file for " + playername, "LuaConfig::CreateNewSave", "config" );

    std::string savegameFile = playername + "-save.lua";

    // Update the amount of save games in the config file
    m_totalSavegames++;
    SetOption( "savegame_count", StringUtil::IntToString( m_totalSavegames ) );

    // Set the "savegame_#" option in config to match the filename for this player
    SetOption( "savegame_" + StringUtil::IntToString(m_totalSavegames), savegameFile );

    // Set what the current active savegame is
    m_currentSavegame = savegameFile;

    Logger::Out( "Savegame file is " + m_currentSavegame, "LuaConfig::CreateNewSave", "config" );

    for ( auto& option: settings )
    {
        // Store all the settings
        SetSaveData( option.first, option.second );
    }

    // Saves the player's save game
    SaveState();

    // Update the config file
    SaveConfig();
}

//! Delete the current save file, remove it from the list in the config file
void LuaConfig::DeleteCurrentSavefile()
{
    remove( m_currentSavegame.c_str() );
    m_currentSavegame = "";

    // Update the amount of save games in the config file
    m_totalSavegames--;
    SetOption( "savegame_count", StringUtil::IntToString( m_totalSavegames ) );

    // Update the config file
    SaveConfig();
}

//! Saves the current save file for a player
void LuaConfig::SaveState()
{
    Logger::Out( "Write out save file \"" + m_currentSavegame + "\"", "LuaConfig::SaveState", "config" );

    std::ofstream out;
    out.open( ("savegames/" + m_currentSavegame).c_str() );

    out << "save = {" << std::endl;

    for ( auto& option: m_saveData )
    {
        out << "\t" << option.first << " = \"" << option.second << "\"," << std::endl;
        Logger::Out( "\t" + option.first + " = " + option.second, "LuaConfig::SaveState", "config" );
    }

    out << "}" << std::endl;

    out.close();
}

//! Loads a save file into the game
/**
@param      filename        The filename of the save game to load
@param      settings        The settings (keys) to load
@return     true if save game is loaded, false if save game cannot be found
*/
bool LuaConfig::LoadState( const std::string& filename, const std::vector<std::string>& settings )
{
    m_currentSavegame = filename;

    if ( !LuaManager::LoadScript( "savegames/" + m_currentSavegame ) )
    {
        return false;
    }

    // Load in values to the key/value
    for ( auto& option: settings )
    {
        SetSaveData( option, LuaManager::Savegame_GetData( option ) );
    }

    return true;
}

//! Retrieve the value of a save game's option
/**
@param      key     The key of the save game option
@return             The value of the save game option
*/
std::string LuaConfig::GetSaveData( const std::string& key )
{
    return m_saveData[ key ];
}

//! Sets an option in the save game data
/**
@param      key     The key of the option
@param      val     The value of the option
*/
void LuaConfig::SetSaveData( const std::string& key, const std::string& val )
{
    Logger::Out( "Set save data \"" + key + "\" to value \"" + val + "\".", "LuaConfig::SetSaveData", "config" );
    m_saveData[ key ] = val;
}

//! Get the name of the current save game's name
/**
@return     The current save game's name
*/
std::string LuaConfig::GetSavegameName()
{
    return m_currentSavegame;
}

//! Get the amount of save games available
/**
@return     The current save game's name
*/
int LuaConfig::GetSavegameCount()
{
    return m_totalSavegames;
}

}

#endif
