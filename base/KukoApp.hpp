#ifndef _KUKO_APP
#define _KUKO_APP

#include "Application.hpp"
#include "../utilities/Platform.hpp"
#include "../utilities/Logger.hpp"
#include "../utilities/StringUtil.hpp"
#include "../managers/ImageManager.hpp"
#include "../managers/StateManager.hpp"
#include "../managers/InputManager.hpp"
#include "../managers/FontManager.hpp"
#include "../managers/LanguageManager.hpp"
#include "../managers/LuaManager.hpp"
#include "../managers/SoundManager.hpp"
#include "../managers/ConfigManager.hpp"

#include <iostream>
#include <string>

class KukoApp
{
public:
  KukoApp();
  ~KukoApp();

  void Run();
  void Cleanup();
  
protected:
  void Setup();
  void KukoSetup();
  void AppSetup();

  std::string m_contentPath;
  bool m_gotoLanguageSelect;
  
  static std::string m_gotoLevel;
}

#endif
